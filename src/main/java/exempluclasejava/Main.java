package exempluclasejava;

public class Main {
    public static void main(String[] args) {
        Integer x = new Integer(5);
        Integer z = 3;
        System.out.println(x.compareTo(z));
        int a;

         // declarare :Laptop varOne;
        //instantiere, initializare
        Laptop varOne = new Laptop();
        varOne.cpu = 2.4;
        varOne.memorie = 4;
        varOne.display = 15.4;
        varOne.os = "Windows";

        System.out.println("Laptopul meu este: " + varOne.os + " si are " + varOne.memorie + " RAM " + varOne.cpu + " GHz si" +
                " un display de " + varOne.display);

        Laptop varTwo = new Laptop();
        varTwo.cpu = 3.0;
        varTwo.memorie = 8;
        varTwo.display = 15.4;
        varTwo.os = "macOS";

        System.out.println("Laptopul meu este: " + varTwo.os + " si are " + varTwo.memorie + " RAM " + varTwo.cpu + " GHz si" +
                " un display de " + varTwo.display);

     }
}
