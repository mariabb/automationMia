package staticex;


public class Variables {

    private static  String TEXT = "The end.";

    public static void main(String[] args) {
        System.out.println("Hi!");
        String s = "Hi!";
        System.out.println(s);
        if (args.length>0){
            String s2 =s;
            System.out.println("am intrat in if");
            
        }
        Variables variables = new Variables();
        System.out.println(variables.instanceVariable);
        System.out.println(TEXT);
    }
    
    public String instanceVariable;
    public Variables(){
        instanceVariable = "Instance variable test";
    }
}
//outputu
//Hi!
//        Hi!
//        Hi!
//        Instance variable test
    //    The end.
