package staticex;
//https://docs.oracle.com/javase/tutorial/java/javaOO/QandE/creating-answers.html
public class IdentifyMyParts {
    public static int x = 7; //class variable;static variable, shared across all instances of the class.
    //That is, there is only one x: when the value of x changes in any instance it affects
    // the value of x for all instances of IdentifyMyParts.
    public int y = 3; //instance variable


    public static void main(String[] args) {
        IdentifyMyParts a = new IdentifyMyParts();
        IdentifyMyParts b = new IdentifyMyParts();
        a.y = 10;
        b.y = 12;
//        a.x = 5;
//        b.x = 8;

        x = 5;
        x = 8;
        System.out.println("a.y = " + a.y);
        System.out.println("b.y = " + b.y);
        System.out.println("a.x = " + x);
        System.out.println("b.x = " + x);
        System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x);
//output:a.y = 10
//b.y = 12
//a.x = 8
//b.x = 8
//IdentifyMyParts.x = 8
    }
}