package staticex;

public class Main {
    public int count =0;

    public void run() {
        count = 15;
        int count = 10;
        count++;
    }
//"Member variables must also be unique for each class."
//
//"But there is an exception: the names of local variables and member variables can be identical."
    //"This code declares two count variables. Line 4 declares an instance variable, and line 7 – a local variable."

}

